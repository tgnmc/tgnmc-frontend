var express = require('express');
    player = require('./routes/player');
    rank = require('./routes/rank');
    server = require('./routes/server');
    binus = require('./routes/binus');
    gojek = require('./routes/gojek')
    testproject = require('./routes/test-project');

var fs = require('fs');
var app = express();
var _ = require('underscore')._;
var gulp = require('gulp');
var sitemap = require('express-sitemap');
var bodyParser = require('body-parser');

var config = {
  key: fs.readFileSync("./ssl/host-nopwd.key"),
  cert: fs.readFileSync("./ssl/STAR_tgnmc_com.crt")
};

var https = require('http').createServer(app);
var html = require('html');

var Pusher = require('pusher');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

var mongo = require('mongodb');
var multer  = require('multer');
var upload = multer({ dest: './uploads/'});

var Server = mongo.Server,
    Db = mongo.Db;

var BSON = require('bson');

var serverss = new Server('db.tgnmc.com', 27017, {auto_reconnect: true});
db = new Db('TGNDB', serverss);

db.open(function(err, db) {
  if (!err) {
    console.log('Connected to TGNDB database');
  }
});

var mongoose = require('mongoose');
var passport = require('passport');
var session      = require('express-session');
var flash    = require('connect-flash');

mongoose.connect("mongodb://db.tgnmc.com/passport");

require('./routes/passport')(passport);




// VARIABLES
var players = {};
var servers = {};
var pusher = new Pusher({ appId: "130729", key: "c746ef932601eb71dc05", secret:  "667f6cbdf31cc7fd68dc" });

app.set('view engine', 'ejs'); // set up ejs for templating

// INITIAL
app.use(express.static(__dirname + '/public'));
app.use('/js', express.static(__dirname + '/js'));
app.use('/img', express.static(__dirname + '/img'));
app.use('/css', express.static(__dirname + '/css'));
app.use('/hashed', express.static(__dirname + '/hashed'));
app.use('/uploads', express.static(__dirname + '/uploads'));

app.use(session({secret: 'hahahahahaahahahahahahahahahaahahahahah'})); //nice session secret, morpig!
app.use(passport.initialize());
app.use(passport.session());
app.use(flash()); // use connect-flash for flash messages stored in session



// normal routes ===============================================================

    // PROFILE SECTION =========================
    app.get('/space', isLoggedIn, function(req, res) {
      //res.sendFile(__dirname + '/public/' + 'test-project.html');
      console.log(req.user.local.email);
      res.render('test-project', { user: req.user });
    });

    // LOGOUT ==============================
    app.get('/logout', function(req, res) {
        req.logout();
        res.redirect('/space');
    });

// =============================================================================
// AUTHENTICATE (FIRST LOGIN) ==================================================
// =============================================================================

    // locally --------------------------------
        // LOGIN ===============================
        // show the login form
        app.get('/login', function(req, res) {
            res.render('login.ejs', { message: req.flash('loginMessage') });
        });

        // process the login form
        app.post('/login', passport.authenticate('local-login', {
            successRedirect : '/space', // redirect to the secure profile section
            failureRedirect : '/login', // redirect back to the signup page if there is an error
            failureFlash : true // allow flash messages
        }));

        // SIGNUP =================================
        // show the signup form
        app.get('/signup', function(req, res) {
            res.render('signup.ejs', { message: req.flash('signupMessage') });
        });

        // process the signup form
        app.post('/signup', passport.authenticate('local-signup', {
            successRedirect : '/space', // redirect to the secure profile section
            failureRedirect : '/signup', // redirect back to the signup page if there is an error
            failureFlash : true // allow flash messages
        }));


// =============================================================================
// AUTHORIZE (ALREADY LOGGED IN / CONNECTING OTHER SOCIAL ACCOUNT) =============
// =============================================================================

    // locally --------------------------------
        app.get('/connect/local', function(req, res) {
            res.render('connect-local.ejs', { message: req.flash('loginMessage') });
        });
        app.post('/connect/local', passport.authenticate('local-signup', {
            successRedirect : '/profile', // redirect to the secure profile section
            failureRedirect : '/connect/local', // redirect back to the signup page if there is an error
            failureFlash : true // allow flash messages
        }));

// =============================================================================
// UNLINK ACCOUNTS =============================================================
// =============================================================================
// used to unlink accounts. for social accounts, just remove the token
// for local account, remove email and password
// user account will stay active in case they want to reconnect in the future

    // local -----------------------------------
    app.get('/unlink/local', isLoggedIn, function(req, res) {
        var user            = req.user;
        user.local.email    = undefined;
        user.local.password = undefined;
        user.save(function(err) {
            res.redirect('/profile');
        });
    });




// API STUFFS

app.get('/api', function(req, res) {
  res.send({error: "What are you doing here?", version: "0.0.1", publisher: "The Gamers Network"});
})

app.get('/api/test-project', testproject.getList);

app.get('/api/player', player.findAll);
app.get('/api/player/:id', player.findByUUID);
app.get('/api/server', server.findAll);
app.get('/api/server/:id', server.findByUUID);
app.get('/api/server/finder/:id', server.findByPort);
app.get('/api/rank', rank.findAll);
app.get('/api/rank/:id', rank.findByUUID);
app.put('/api/rank/:id', rank.updateRank);
app.get('/api/loves', binus.getList);

//test project
app.get('/testproject', function(req, res) {
  res.sendFile(__dirname + '/public/' + 'test-project.html');
});

app.get('/delete/:id', function(req, res) {
  var id = req.params.id;
  var collection = db.collection('test-project');
  collection.deleteOne({
    "imageurl": id
  }, function(err, results) {
    res.redirect('/space');
  });

})

//gojek api
app.get('/api/proto/gojek', gojek.findAll);
app.get('/api/proto/gojek/customer/:id', gojek.findByUUID);
app.get('/api/proto/gojek/customer/history/:id', gojek.findHistory);

app.get('/api/love', function(req, res) {
  res.sendFile(__dirname + '/public/' + 'binus-stats.html');
});
app.post('/love', function(req, res) {
  var collection = db.collection('binus');
  var body = req.body;
});

var fs = require('fs');
app.post('/test', upload.single('imageurl'), function(req, res) {
  var body = req.body;
  var title = body.title;
  var description = body.description;

  var imageurl = req.file.filename;

  console.log(req.file);
  console.log(req.user);

  var user = req.user.local;
  var email = user.email;

  console.log(user);
  console.log(email);

  var collection = db.collection('test-project');
  collection.insert({
    "title": title,
    "description": description,
    "imageurl": imageurl,
    "user": email
  }, function (err, doc) {
    if (err) {
      res.send("Database Error!")
    } else {
      res.redirect('/space');
    }
  });

});

function isLoggedIn(req, res, next) {
    if (req.isAuthenticated())
        return next();

    res.redirect('/login');
}



// LIVE MUSIC - TGNMC V2.0
app.get('/music', function(req, res) {
  res.sendFile(__dirname + '/public/' + 'music.html');
});

// ERROR REPORTING
app.get('/portal', function(req, res) {
  res.sendFile(__dirname + '/public/' + 'admin.html');
});

app.get('/stats', function(req, res) {
  res.sendFile(__dirname + '/public/' + 'stats.html');
});
// BINUS FUN HEHE.
app.get('/love', function(req, res) {
  res.sendFile(__dirname + '/public/' + 'binus.html');
});

app.get('/match/game', function(req, res) {
  res.sendFile(__dirname + '/public/' + 'game-match.html');
})

app.post('/test/payment', function(req, res) {
  console.log(req.body);
});

app.post('/pusher/auth', function(req, res) {
  var socketId = req.body.socket_id;
  var channel = req.body.channel_name;
  var auth = pusher.authenticate(socketId, channel);
  res.send(auth);
});

var map = sitemap({
  http: "https",
  url: "tgnmc.com",
  generate: app
});

app.get('/sitemap.xml', function(req, res) { // send XML map

  map.XMLtoWeb(res);
}).get('/robots.txt', function(req, res) { // send TXT map

  map.TXTtoWeb(res);
});


https.listen(process.env.PORT || 8080);
