var socket = io("https://ws.tgnmc.com");



var ref = new Firebase("https://tgnmc.firebaseio.com/");

var x = "";
var id = "";

$(document).ready(function () {
  console.log("READY!");
  id = generateID();
  x += "<p>Connect to our server and type <code>/verify " + id + "</code>.</p>";

  delete_cookie("code");
  setCookie("code", id, 365);

  $('#loading').hide();
  $('#gamelist').hide();
  $('#code').html(x);

  //DEMO
  var table = "";
  table += "<thead><tr><th>Server Name</th><th>Players</th><th>Starts At</th></tr>";
  table += "</thead>";
  table += "<tbody><tr class='warning'><td>MORPIG</td><td>1</td><td>150</td></tr>";
  $('#gamelist-table').html(table);

});

//cokie test
setCookie("test", "morpigpro", 365);

socket.on('code confirm', function(code, uuid) {
  console.log("hehe");
  console.log(code);
  console.log(uuid);
  if (code == id) {
    console.log("correct!");
    $('#status').html('<h1>Loading!.....');
    $('#steve').attr('src', 'https://visage.gameminers.com/bust/513/' + uuid);
    $('#register').hide();
    $('#loading').toggle();
    setCookie("uuid", uuid, 365);
  } else {
    console.log("not correct!");
    $('#status').html('<h1>Still waiting.....');
  }
});

function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+d.toUTCString();
    document.cookie = cname + "=" + cvalue + "; " + expires;
}

var delete_cookie = function(name) {
    document.cookie = name + '=;expires=Thu, 01 Jan 1970 00:00:01 GMT;';
};

function generateID() {
   var text = "";
   var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

   for( var i=0; i < 5; i++ )
       text += possible.charAt(Math.floor(Math.random() * possible.length));

   return text;
}
