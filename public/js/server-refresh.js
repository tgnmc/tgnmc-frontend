var socket = io("https://ws.tgnmc.com");
var title = null;
var music = null;

var globalVolume = 100;

console.log('Initial Connection!!');

soundManager.setup({

});

socket.on("danger", function(serverName) {

    if (typeof(soundManager) !== 'undefined') {
      //soundManager.stopAll();
    } else {
      var music = soundManager.createSound({
        url: 'https://dgfpfwihfexkg.cloudfront.net/music/' + musicid + '.mp3',
      }).play();
    }

});

$(document).ready(function () {
  console.log("READY!");
    populate();
    window.setInterval(populate, 3000);
});

function populate() {
  console.log("populating!");
    $.getJSON('/api/server', function (data) {
      console.log(data.length);
      $('#server-online').html("<h3>Server Online: " + data.length + "</h3>");
      var x = "";
      x += "<div class=\"row align-row\">";

      x += "<h4> Servers </h4>";
      $.each(data, function(i, field) {
        console.log(field);
        console.log(field.serverName);

        if (field.online == true) {
          if (field.danger == true) {
            io.emit("danger", field.serverName);
            x += "<span class=\"btn btn-warning btn-override\">" +
                          field.serverName + "<em>" + field.ip + "/" + field.port + "</em></span>";
            x += "</div>"
          } else {
            x += "<span class=\"btn btn-success btn-override\">" +
                          field.serverName + "<em>" + field.ip + "/" + field.port + "</em></span>";
            x += "</div>"
          }
        } else {
          x += "<span class=\"btn btn-danger btn-override\">" +
                        field.serverName + "<em>" + field.ip + "/" + field.port + "</em></span>";
          x += "</div>"
        }
      });
      $('#alive-data').html(x);
    });
  };
