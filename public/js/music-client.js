var socket = io("https://ws.tgnmc.com");
var title = null;
var music = null;

var globalVolume = 100;

console.log('Initial Connection!!');

soundManager.setup({

});

if (!("Notification" in window)) {
  alert("TTAM Notifications are supported in modern versions of Chrome, Firefox, Opera and Firefox.");
}

//Check if user is okay to get some notif
else if (Notification.permission === "granted") {

}

//Otherwise, ask user for permission
else if (Notification.permission !== 'denied') {
  Notification.requestPermission(function (permission) {
    //If the user is okay, create a notification!
    if (permission === "granted") {

    }
  })
}

//initial for notifications
socket.on('initial connection', function() {
  console.log('Initial Connection!!');
  //send permissions
  //give the notifications to chrome or any html5 browsers
  if (!Notification) {
    alert('TGNv2 Notifications are supported in modern versions of Chrome, Firefox, Opera and Firefox.');
    return;
  }

  if (Notification.permission !== "granted")
  Notification.requestPermission();

});



socket.on('play music', function(musicid, title) {

  //get track name and give notifications

  if (typeof(soundManager) !== 'undefined') {
    soundManager.stopAll();
  }

  var music = soundManager.createSound({
    url: 'https://dgfpfwihfexkg.cloudfront.net/music/' + musicid + '.mp3',
  }).play();

  $('#nowplaying').html("Now Playing: " + title);
  var notification = new Notification('The Gamers Network v2', {
      icon: 'http://es-1.gamersnetworkmc.com/official-alt.png',
      body: 'Now Playing: ' + title,
    });


});

socket.on('disconnected', function() {
  console.log("Lost connection to Socket.IO server. will be trying to reconnect.....");
});

socket.on("update", function(msg) {
  $("#msgs").append("<li>" + msg + "</li>");
  console.log("Connected to TGNv2 websocket");
});

socket.on("exists", function(data) {
  $("#errors").empty();
  $("#errors").show();
  $("#errors").append(data.msg);
  $("#login-screen").toggle();
  $("#music-controls").toggle();
});

$(document).ready(function() {


  $("form").submit(function(event) {
    event.preventDefault();
  });

  $("#errors").hide();
  $("#music-controls").hide();


  $("#nameForm").submit(function() {
    console.log("1");
    var name = $("#username").val();
    var device = "desktop";
    if (name === "" || name.length < 2) {
      console.log("The username is " + name);
      $("#errors").empty();
      $("#errors").append("Please enter a name");
      $("#errors").show();
    } else {
      $("#errors").hide();
      console.log("The username is  " + name);
      socket.emit("joinserver", name, device);
      $("#warning").toggle();
      $("#login-screen").toggle();
      $("#music-controls").toggle();

    }
  });

  /*
  $('form').submit(function() {
    socket.emit('play music', $('#m').val());
    console.log("I've send the information to websocket, which is " + $('#m').val());
    $('m').val('');
    return false;
  });
  */

  // Without JQuery
  var slider = new Slider('#ex1', {
  	formatter: function(value) {
      console.log(value);
      if (typeof(soundManager) !== 'undefined') {
        soundManager.setVolume(value);
      }
      return value + '%';
  	}
  });

});
