var ref = new Firebase("https://tgnmc.firebaseio.com");

var gojek = ref.child("gojek");

//import cs data (example)
gojek.push(
  {
    "id": "1", //id as string karena firebase blm support
    "name": "morpig",
    "phone": "081932251470",
    "email": "morpig@tgnmc.com",
    "corporateId": null,
    "corporateName": null,
    "creditBalance": "0.00",
    "referralCreditBalance": null,
    "newCustomer": false,
    "blacklisted": false,
    "signupDate": false,
    "history": [
      {
        "name":"Jalan Kemang Selatan XII",
        "address":null,
        "latLong":"-6.273309737873747,106.81386340409517",
        "note":"deket 7-11 - telp  0818144483"
      },
      {
        "name":"Jalan Saari",
        "address":null,
        "latLong":"-6.20401729862148,106.77111402153969",
        "note":""
      }
    ]
  }
);
