module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    less: {
        main: {
            files: {
                "css/julatic.css": "css/julatic.less"
            }
        },
        colorFFD23A: {
            options: {
                modifyVars: {
                    'main-color': '#FFD23A'
                }
            },
            files: {
                "css/julatic-FFD23A.css": "css/julatic.less"
            }
        },
        colorB38853: {
            options: {
                modifyVars: {
                    'main-color': '#B38853'
                }
            },
            files: {
                "css/julatic-B38853.css": "css/julatic.less"
            }
        },
        colorE42242: {
            options: {
                modifyVars: {
                    'main-color': '#E42242'
                }
            },
            files: {
                "css/julatic-E42242.css": "css/julatic.less"
            }
        },
        color775CAB: {
            options: {
                modifyVars: {
                    'main-color': '#775CAB'
                }
            },
            files: {
                "css/julatic-775CAB.css": "css/julatic.less"
            }
        },
        color29D3DE: {
            options: {
                modifyVars: {
                    'main-color': '#29D3DE'
                }
            },
            files: {
                "css/julatic-29D3DE.css": "css/julatic.less"
            }
        },
        color29DE73: {
            options: {
                modifyVars: {
                    'main-color': '#29DE73'
                }
            },
            files: {
                "css/julatic-29DE73.css": "css/julatic.less"
            }
        },
        color3A9BFF: {
            options: {
                modifyVars: {
                    'main-color': '#3A9BFF'
                }
            },
            files: {
                "css/julatic-3A9BFF.css": "css/julatic.less"
            }
        },
        mainDark: {
            files: {
                "css/julatic-dark.css": "css/julatic.less"
            }
        },
        colorFFD23ADark: {
            options: {
                modifyVars: {
                    'main-color': '#FFD23A',
                    'header-color': '#171819'
                }
            },
            files: {
                "css/julatic-FFD23A-dark.css": "css/julatic.less"
            }
        },
        colorB38853Dark: {
            options: {
                modifyVars: {
                    'main-color': '#B38853',
                    'header-color': '#171819'
                }
            },
            files: {
                "css/julatic-B38853-dark.css": "css/julatic.less"
            }
        },
        colorE42242Dark: {
            options: {
                modifyVars: {
                    'main-color': '#E42242',
                    'header-color': '#171819'
                }
            },
            files: {
                "css/julatic-E42242-dark.css": "css/julatic.less"
            }
        },
        color775CABDark: {
            options: {
                modifyVars: {
                    'main-color': '#775CAB',
                    'header-color': '#171819'
                }
            },
            files: {
                "css/julatic-775CAB-dark.css": "css/julatic.less"
            }
        },
        color29D3DEDark: {
            options: {
                modifyVars: {
                    'main-color': '#29D3DE',
                    'header-color': '#171819'
                }
            },
            files: {
                "css/julatic-29D3DE-dark.css": "css/julatic.less"
            }
        },
        color29DE73Dark: {
            options: {
                modifyVars: {
                    'main-color': '#29DE73',
                    'header-color': '#171819'
                }
            },
            files: {
                "css/julatic-29DE73-dark.css": "css/julatic.less"
            }
        },
        color3A9BFFDark: {
            options: {
                modifyVars: {
                    'main-color': '#3A9BFF',
                    'header-color': '#171819'
                }
            },
            files: {
                "css/julatic-3A9BFF-dark.css": "css/julatic.less"
            }
        },
    }
  });

  // Load the plugin that provides the "uglify" task.
  grunt.loadNpmTasks('grunt-contrib-less');

  // Default task(s).
  grunt.registerTask('default', ['less']);

};