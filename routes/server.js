var mongo = require('mongodb');

var Server = mongo.Server,
    Db = mongo.Db;

var BSON = require('bson');

var server = new Server('db.tgnmc.com', 27017, {auto_reconnect: true});
db = new Db('TGNDB', server);

db.open(function(err, db) {
  if (!err) {
    console.log('Connected to TGNDB database');
  }
});

exports.findAll = function(req, res) {
  db.collection('server-proto', function(err, collection) {
        collection.find().toArray(function(err, items) {
            res.send(items);
        });
    });
};

exports.findByUUID = function(req, res) {
  var id = req.params.id;
      console.log('Retrieving server: ' + id);
      db.collection('server-proto', function(err, collection) {
          collection.findOne({'id': id}, function(err, item) {
              res.send(item);
          });
      });
};

exports.findByPort = function(req, res) {
  var id = req.params.id;
      console.log('Retrieving server: ' + id);
      db.collection('server-proto', function(err, collection) {
          collection.findOne({'port': id}, function(err, item) {
              res.send(item);
          });
      });
};
