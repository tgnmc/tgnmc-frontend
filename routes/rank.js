var mongo = require('mongodb');

var Server = mongo.Server,
    Db = mongo.Db,
    BSON = mongo.BSONPure;

var server = new Server('db.tgnmc.com', 27017, {auto_reconnect: true});
db = new Db('TGNDB', server);

db.open(function(err, db) {
  if (!err) {
    console.log('Connected to TGNDB database');
  }
});

exports.findAll = function(req, res) {
  db.collection('permissionDataP', function(err, collection) {
        collection.find().toArray(function(err, items) {
            res.send(items);
        });
    });
};

exports.findByUUID = function(req, res) {
  var id = req.params.id;
      console.log('Retrieving uuid: ' + id);
      db.collection('permissionDataP', function(err, collection) {
          collection.findOne({'uuid': id}, function(err, item) {
              res.send(item);
          });
      });
};

exports.updateRank = function(req, res) {
  var id = req.params.id;
  var rank = req.body;

  db.collection('permissionDataP', function(err, collection) {
      collection.findOne({'uuid': id}, function(err, item) {
        console.log(item);
        console.log(item._id);
        console.log('Updating rank with uuid: ' + id);
        console.log(JSON.stringify(rank));
        var itemid = item._id;
        db.collection('permissionDataP', function(err, collection) {
          collection.update({'_id': BSON.ObjectID(itemid)}, rank, {safe: true}, function(err, result) {
            if (err) {
              console.log('Error updating rank with uuid: ' + id);
              res.send({"error": "An error has occurred"});
            } else {
              console.log('' + result + ' document(s) updated');
              res.send(rank);
            }
          });
        });
      });
  });
};
